/**
 * Created by Jordan Chalupka on 9/19/2017.
 */

const AWS = require('aws-sdk');
const athena = new AWS.Athena();
const co = require('co');


// Athena Helper
class AthenaHelper {
    constructor(config) {
        this.config = {
            output_location: config.output_location,
            wait: config.wait,
            max_retry_count: config.max_retry_count,
        };
    }

    /**
     *
     * @param query
     * @param database
     * @returns {Promise}
     */
    startQuery(query, database) {
        const self = this;
        const {
            output_location,
            max_retry_count,
            wait
        } = self.config;

        return new Promise((resolve, reject) => {
            let retry_count = 0;
            let params = {
                QueryString: query,
                ResultConfiguration: {
                    /* required */
                    OutputLocation: output_location, /* required */
                    EncryptionConfiguration: {
                        EncryptionOption: 'SSE_S3', /* required */
                    }
                }
            };

            if(database) {
                params.QueryExecutionContext = {
                    Database: database
                };
            }

            function loop() {
                athena.startQueryExecution(params, (err, data) => {
                    if (err) {
                        // check if we can retry
                        if (isRetryException(err) && canRetry(retry_count, max_retry_count)) {
                            retry_count++;
                            return setTimeout(loop, wait);
                        }
                        else {
                            return reject(err);
                        }
                    }

                    return resolve(data.QueryExecutionId);
                });
            }

            loop();
        });
    }

    /**
     *
     * @param query_id
     * @returns {Promise}
     */
    checkQuery(query_id) {
        const self = this;
        const {
            max_retry_count,
            wait
        } = self.config;

        return new Promise((resolve, reject) => {
            let retry_count = 0;
            const params = {
                QueryExecutionId: query_id
            };

            function loop() {
                athena.getQueryExecution(params, (err, data) => {
                    if (err) {
                        // check if we can retry
                        if (isRetryException(err) && canRetry(retry_count, max_retry_count)) {
                            retry_count++;
                            return setTimeout(loop, wait);
                        }
                        else {
                            return reject(err);
                        }
                    }

                    const state = data.QueryExecution.Status.State;

                    let isEnd;
                    let error;
                    switch (state) {
                        case 'QUEUED':
                        case 'RUNNING':
                            isEnd = false;
                            break;
                        case 'SUCCEEDED':
                            isEnd = true;
                            break;
                        case 'FAILED':
                            isEnd = false;
                            error = new Error('FAILED: Execution Error ' + data.QueryExecution.Status.StateChangeReason);
                            break;
                        case 'CANCELLED':
                            isEnd = false;
                            error = new Error('FAILED: Execution Error ' + data.QueryExecution.Status.StateChangeReason);
                            break;
                        default:
                            isEnd = false;
                            error = new Error(`FAILED: UnKnown State ${state}` + data.QueryExecution.Status.StateChangeReason)
                    }

                    if (error) {
                        return reject(error);
                    }
                    if (isEnd) {
                        return resolve(true);
                    }
                    return resolve(false);
                });
            }

            loop()
        });
    }

    /**
     *
     * @param query_id
     * @param nextToken
     * @returns {Promise}
     */
    getQueryResults(query_id, nextToken) {
        const self = this;
        const {
            wait,
        } = self.config;

        let {
            max_retry_count
        } = self.config;

        return new Promise((resolve, reject) => {
            let retry_count = 0;
            let params = {
                QueryExecutionId: query_id
            };

            // results are greater than 1000
            if (nextToken) {
                params.NextToken = nextToken;
            }

            function loop() {
                athena.getQueryResults(params, (err, data) => {
                    if (err) {
                        // check if we can retry
                        if (isRetryException(err) && canRetry(retry_count, max_retry_count)) {
                            max_retry_count++;
                            return setTimeout(loop, wait);
                        }
                        else {
                            return reject(err);
                        }
                    }

                    return resolve(data);
                });
            }

            loop();
        });
    }

    /**
     *
     * @param query
     * @param database
     * @param format
     * @returns {Promise}
     */
    execute(query, database, format) {
        let self = this;
        const {
            wait
        } = self.config;

        return new Promise((resolve, reject) => {
            return co(function*() {
                try {
                    // start the query
                    let query_id = yield self.startQuery(query, database);

                    while (true) {
                        yield sleep(wait);
                        const isEnd = yield self.checkQuery(query_id);
                        if (!isEnd) {
                            continue;
                        }

                        let data = yield self.getQueryResults(query_id);

                        let previousToken = undefined;
                        while (data.NextToken && data.NextToken !== previousToken) {
                            let dataTmp = yield self.getQueryResults(query_id, data.NextToken);
                            data.NextToken = dataTmp.NextToken || null;
                            data.ResultSet.Rows = data.ResultSet.Rows.concat(dataTmp.ResultSet.Rows);
                            previousToken = data.NextToken;
                        }

                        // extract the data
                        data = extractData(data, format);

                        // query is finished
                        return resolve(data);
                    }
                }
                catch(err) {
                    reject(err);
                }
            })
        });
    }
}

/**
 *
 * @param err
 * @returns {boolean}
 */
function isRetryException(err) {
    return err.code === 'ThrottlingException' || err.code === 'TooManyRequestsException'
}

/**
 *
 * @param retryCount
 * @param max_retry
 * @returns {boolean}
 */
function canRetry(retryCount, max_retry) {
    return retryCount < max_retry;
}

/**
 *
 * @param data
 * @param format
 * @returns {*}
 */
function extractData(data, format) {
    let result;
    let rows;
    switch (format) {
        case 'raw':
            result = data;
            break;
        case 'array':
            result = [];
            rows = data.ResultSet.Rows;
            if (rows && rows.length !== 0) {
                let cols = rows.shift().Data;
                let len = rows.length;
                for (let i = 0; i < len; i++) {
                    let row = rows[i].Data;
                    let record = {};
                    for (let j = 0; j < row.length; j++) {
                        let colName = cols[j].VarCharValue;
                        record[colName] = row[j].VarCharValue;
                    }
                    result.push(record);
                }
            }
            break;
        case 'array-no-key':
            result = [];
            rows = data.ResultSet.Rows;
            if (rows && rows.length !== 0) {
                let len = rows.length;
                for (let i = 0; i < len; i++) {
                    let row = rows[i].Data;
                    for (let j = 0; j < row.length; j++) {
                        result.push(row[j].VarCharValue);
                    }
                }
            }
            break;

        default:
            throw new Error(`invalid format ${format}`);
    }
    return result
}

/**
 *
 * @param time
 * @returns {Promise}
 */
function sleep(time) {
    return new Promise((resolve) => setTimeout(resolve, time));
}

module.exports = AthenaHelper;
