'use strict';

const AthenaHelper = require('./AthenaHelper');
const moment = require('moment');

function search_all_databases(databases) {
    return databases.length === 1 && databases[0] === '*';
}

function getSchemaTables(athena_helper, databases) {
    const show_tables = 'SHOW TABLES';

    const show_tables_promises = databases.map((database) => {
        return athena_helper.execute(show_tables, database, 'array-no-key');
    });

    return Promise.all(show_tables_promises)
        .then(show_tables_result => {
            // create an object with the database and tables
            return show_tables_result.map((table_names, index) => {
                const tables = table_names.map((table_name) => {
                    return {
                        table_name: table_name
                    };
                });

                return {
                    database_name: databases[index],
                    tables: tables
                }
            });
        });
}


function getCreateStatement(athena_helper, table, database_name) {
    // get the create statement for the table
    const show_create_tables = `SHOW CREATE TABLE ${table.table_name}`;
    return athena_helper.execute(show_create_tables, database_name, 'array-no-key');
}


function getCreateStatements(athena_helper, schema_tables) { // TODO there is a problem in here
    return new Promise((resolve, reject) => {
        // get all of the create statements for a database
        const show_create_promises_for_all_databases = schema_tables
            .map((database) => {
                const show_create_promises_for_database = database.tables
                    .map((table) => {
                        return getCreateStatement(athena_helper, table, database.database_name);
                    });

                return Promise.all(show_create_promises_for_database)
                    .then(create_statements => {
                        database.tables = database.tables
                            .map((table, table_index) => {
                                const create_statement = create_statements[table_index].join('');

                                // table.create = create_statements[table_index];
                                table.has_partitions = create_statement.includes('PARTITIONED BY');
                                // table.partitioned_by = [];

                                return table;
                            });

                        return database;
                    })
                    .catch(err => reject(err));
            });

        Promise.all(show_create_promises_for_all_databases)
            .then(data => {
                resolve(data);
            });
    });
}


function getPartitionedTables(athena_helper, databases) {
    return new Promise((resolve, reject) => {
        getSchemaTables(athena_helper, databases)
            .then(schema_tables => getCreateStatements(athena_helper, schema_tables))
            .then(data => resolve(data))
            .catch(err => reject(err));
    });
}

function filterUnpartitionedTables(databases) {
    return databases
        .filter(database => {
            console.log('database');
            console.log(database);
            database.tables = database.tables
                .filter(table => {
                    console.log('table');
                    console.log(table);
                    return table.has_partitions;
                });

            console.log('after filter');
            console.log(database.tables);

            return database.tables.length > 0;
        })
}

function getValue(column, index) {
    switch(column.type) {
        case 'date':
            // get the date value
            return moment().add(index, 'day').format(column.format);
            break;

        case 'const':
            return column.value;
            break;
    }
}

/**
 * Get the date for today and the next n days
 *
 * @param partition_columns
 * @param num_days
 */
function getNextDays(partition_columns, num_days) {
    let days = new Array(num_days).fill(undefined);

    // go through each date
    return days.map((day, index) => {
        // go through each column
        return partition_columns.map((column) => {
            const value = getValue(column, index);

            return {
                name: column.name,
                value: value
            };


        });
    });
}

function addPartitions(databases, athena_helper, partition_columns, num_days, location) {
    return new Promise((resolve, reject) => {
        const database_promises = databases.map(database => {
            return new Promise((resolve, reject) => {
                const table_promises = database.tables.map(table => {
                    return new Promise((resolve, reject) => {
                        // get the args for the next days
                        const next_days = getNextDays(partition_columns, num_days);

                        let partition_strings = next_days.map((current_day, index) => {
                            const args_string = current_day.map((arg_value) => {
                                return `${arg_value.name} = '${arg_value.value}'`;
                            }).join(', ');
                            let partition = `PARTITION (${args_string})`;
                            if(location !== undefined) {
                                // get the dates
                                const date = moment().add(index, 'day');
                                const year = date.format('YYYY');
                                const month = date.format('MM');
                                const day = date.format('DD');

                                location = location.replace('{year}', year);
                                location = location.replace('{month}', month);
                                location = location.replace('{day}', day);

                                location = location.replace('{table}', table.table_name);
                                location = location.replace('{database}', database.database_name);

                                current_day.map(vars => {
                                    location = location.replace('{' + vars.name + '}', vars.value);
                                });

                                partition += ` LOCATION '${location}'`
                            }

                            return partition;
                        }).join(' ');

                        const query = `ALTER TABLE ${table.table_name} ADD IF NOT EXISTS ${partition_strings}`;
                        console.log("QUERY: ", query);

                        return athena_helper.execute(query, database.database_name, 'array-no-key')
                            .then(data => resolve(data))
                            .catch(err => {
                                reject(err)
                            });
                    })

                });

                Promise.all(table_promises)
                    .then(data => {
                        resolve(data)
                    })
                    .catch(err => reject(err));
            });

 });

 Promise.all(database_promises)
 .then(() => {

                // send back the databases
                resolve(databases)
            })
 .catch(err => reject(err));
 });

 }

 /**
 * Use the class
 */
exports.handler = (event, context, callback) => {
    const {
        DATABASES,
        PARTITION_COLUMNS,
        NUM_DAYS_TO_PARTITION,
        LOCATION,
        OUTPUT_LOCATION
    } = process.env;

    /**
     * Defaults
     */
    const DEFAULT_PARTITION_COLUMNS = [{
        "name": "pdate",
        "type": "date",
        "format": "YYYY-MM-DD"
    }];

    const DEFAULT_NUM_DAYS_TO_PARTITION = 5;

    let databases;
    let output_location;
    let partition_columns;
    let num_days_to_partition;
    let location;

    if (DATABASES === undefined) {
        callback(new Error('Missing required parameter in event object DATABASES.'), null);
    }
    else {
        databases = JSON.parse(DATABASES);
    }

    if (OUTPUT_LOCATION === undefined) {
        callback(new Error('Missing required parameter in environment object OUTPUT_LOCATION.'), null);
    }
    else {
        output_location = OUTPUT_LOCATION;
    }


    if (PARTITION_COLUMNS === undefined) {
        console.log('PARTITION_COLUMNS was not defined.  Using default "pdate"');
        partition_columns = DEFAULT_PARTITION_COLUMNS;
    }
    else {
        partition_columns = JSON.parse(PARTITION_COLUMNS);
    }

    if (NUM_DAYS_TO_PARTITION === undefined) {
        console.log('NUM_DAYS_TO_PARTITION was not defined.  Setting to 5 days.');
        num_days_to_partition = DEFAULT_NUM_DAYS_TO_PARTITION;
    }
    else {
        num_days_to_partition = NUM_DAYS_TO_PARTITION;
    }

    if (LOCATION === undefined) {
        location = undefined;
    }
    else {
        location = LOCATION;
    }


    const config = {
        //output_location='s3://gd-3666'
        output_location: output_location,
        wait: 200,
        max_retry_count: 5,
    };

    const athena_helper = new AthenaHelper(config);

    let partitioned_tables;
    if (search_all_databases(databases)) {
        const show_databases = 'SHOW DATABASES';

        partitioned_tables = athena_helper.execute(show_databases, null, 'array-no-key')
            .then((databases) => getPartitionedTables(athena_helper, databases));
    }
    else {
        partitioned_tables = getPartitionedTables(athena_helper, databases);
    }

    partitioned_tables
        .then(databases => filterUnpartitionedTables(databases))
        .then(databases => addPartitions(databases, athena_helper, partition_columns, num_days_to_partition, location))
        .then(data => {
            callback(null, data)

        })
        .catch(err => callback(err));

};

